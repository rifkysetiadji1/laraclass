<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;
class Topic extends Model
{
    protected $table='topics';
    protected $fillable=[
        'topic'
    ];

    public function tugas(){
        return $this->hasMany(Task::class);
    }
}

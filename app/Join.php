<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Kelas;
class Join extends Model
{
    protected $table='kelas_user';
    protected $fillable=[
        'user_id',
        'kelas_id'
    ];
    
}

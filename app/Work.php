<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Work extends Model
{
    protected $table='works';
    protected $primaryKey='task_id';
    protected $fillable=[
        'user_id',
        'kelas_id',
        'task_id',
        'file'
    ];

    public function users(){
        return $this->hasMany(User::class,'id','user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Join;
use App\Topic;
use App\Task;
class Kelas extends Model
{
    protected $table='kelas';
    protected $fillable=[
        'nama',
        'deskripsi',
        'kode_kelas',
        'teacher'
    ];

    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }
    public function pelajar(){
        return $this->belongsTo(Join::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
    
    public function topics(){
        return $this->hasMany(Topic::class);
    }

    public function tasks(){
        return $this->hasMany(Task::class)->orderBy('created_at','desc');
    }
}

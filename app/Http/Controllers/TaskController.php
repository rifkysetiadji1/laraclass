<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Kelas;
use App\Topic;
use App\Task;
use App\Work;
use App\User;
use storage;
use Auth;
class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function file(Request $request){
        $id=$request->input('kelas_id');
        $file=$request->file('file');
        $name=$file->getClientOriginalName();
        if($request->file('file')->isValid()){
            $file_name=$name;
            $upload_path='fileupload';
            $request->file('file')->move($upload_path,$file_name);

            return $file_name;
        }
        return false;
    }
    public function store(Request $request){
        $id=$request->input('kelas_id');
        $kelas_id=Kelas::find($id);
        if($request->hasFile('file')){
            $file=$this->file($request);
        }
        

        $kelas_id->tasks()->create([
            'user_id'=>$request->input('user_id'),
            'judul'=>$request->input('judul'),
            'deskripsi'=>$request->input('deskripsi'),
            'topic_id'=>$request->input('topic'),
            'deadline'=>$request->input('tenggat_waktu'),
            'file'=>$file
        ]);
        return redirect('/show/'.$id);
        // return $result;
    }

    public function show($task){
        $tasks=Task::find($task);
        $work=Work::find($tasks);

        $id_user=auth()->user()->id;
        $kelas=Kelas::find($task);
        $user=User::find($id_user);
        $users=$user->class->first();

        $user=Work::find($task);
        if($user){
            $jumlah_user=$user->count();
            $list_selesai=Work::find($task);
            $workes= $list_selesai->users;
        }
        
        

        return view('kelas.task_detail',compact('tasks','work','users','jumlah_user','users','workes'));
        // return $work;
    }

    public function download($file){
        $files=public_path()."/fileupload/".$file;
        return response()->download($files);
    }

    public function work_file(Request $request){
        $id=$request->input('task_id');
        $file=$request->file('file');
        $ext=$file->getClientOriginalExtension();
        if($request->file('file')->isValid()){
            $file_name=date('YmdHis').".$ext";
            $upload_path='fileupload';
            $request->file('file')->move($upload_path,$file_name);

            return $file_name;
        }
        return false;
    }
    public function work(Request $request){
        $id=$request->input('task_id');
        $kelas_id=$request->input('kelas_id');
        $task=Task::find($id);

        if($request->hasFile('work_file')){
            $file=$this->file($request);
        }
        $task->works()->create([
            'user_id'=>$request->input('user_id'),
            'kelas_id'=>$request->input('kelas_id'),
            'file'=>@$file
        ]);
        return redirect('/show/'.$kelas_id);
       
    }
    public function destroy($id){
        $task=Task::find($id);
        $task->delete();
        return redirect('myclass');
    }
    
}

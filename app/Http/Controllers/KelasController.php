<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\kelas;
use App\User;
use App\Join;
use App\Topik;
use App\Task;
use App\Work;
class KelasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        return view('home');
    }
   public function myclass(){
        $id_user=auth()->user()->id;
        $user=User::find($id_user);
        return view('kelas.myclass')->with('class',$user->class);
   }
   public function classroom(){
        $id_user=auth()->user()->id;
        $user=User::find($id_user);
        $students=$user->kelas;
        return view('kelas.classroom',compact('students')); 
   }
   public function create(){
       $kode_kelas=str_random(5);
       return view('kelas.create',compact('kode_kelas'));
   }
   public function join(){
       return view('kelas.join');
   }
   public function join_kode(Request $request){
    //    $join = new Join;
    //    $join->user_id=auth()->user()->id;
    $kelas_id=$request->input('kode_kelas');
    //    $join->save();
    $id_user=auth()->user()->id;
    $user=User::find($id_user);
    $user->kelas()->attach($kelas_id);
    return redirect('classroom');
   }
   public function store(Request $request){
        $kode_kelas=str_random(5);
        $kelas= new Kelas;
        $kelas->nama=$request->input('nama');
        $kelas->deskripsi=$request->input('deskripsi');
        $kelas->kode_kelas=$kode_kelas;
        $kelas->teacher=$request->input('id_user');
        $kelas->save();
        return redirect('myclass');
        
        // $user=$request->input('id_user');
        // $user->class()->create([
        //     'nama'=>$request->input('nama'),
        //     'deskripsi'=>$request->input('deskripsi'),
        //     'kode_kelas'=>$kode_kelas
        // ]);
        // return redirect('home');
   }
   public function show($id){
        $id_user=auth()->user()->id;
        $kelas=Kelas::findOrFail($id);
        $user=User::find($id_user);
        $users=$user->class->first();
        
        $kelas_id=Kelas::findOrFail($id);
        $topics=$kelas_id->topics;
        $topic_list=$kelas_id->topics->pluck('topic','id');

        $kelas_id=Kelas::findOrFail($id);
        $tasks_list=$kelas_id->tasks;

        $work=Work::all();
        return view('kelas.show',compact('kelas','users','topics','topic_list','tasks_list'));
        // dd($users);
        // return $works;
   }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use Auth;
class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request ){
        $id=$request->input('kelas_id');
        $kelas_id=Kelas::find($id);
        $kelas_id->topics()->create([
            'topic'=>$request->input('topic')
        ]);
        return redirect('/show/'.$id);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Work;
class Task extends Model
{
    protected $table='tasks';
    protected $fillable=[
        'kelas_id',
        'user_id',
        'judul',
        'deskripsi',
        'topic_id',
        'deadline',
        'file'
    ];
    protected $dates=['deadline'];
    public function users(){
        return $this->belongsTo(User::class);
    }

    public function works(){
        return $this->hasMany(Work::class);
    }
}

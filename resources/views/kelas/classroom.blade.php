@extends('layouts.app')

@section('content')
<div class="container">
    @if (count($students)>0)
      <div class="row">
        <?php foreach ($students as $student): ?>
            <div class="col-sm-3">
                <div class="card">
                
                <div class="card-header"><a href="/show/{{$student->id}}">{{$student->nama}}</a></div>
                    <div class="card-body">
                        {{$student->deskripsi}}
                        
                        
                    </div>
                </div>
            </div>
            <br>
        <?php endforeach ?>
     </div>
    @else
    <h2>Tidak ada Kelas</h2>
    @endif
    
</div>
@endsection

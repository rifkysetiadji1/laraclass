@extends('layouts.app')
@section('content')
    <div class="jumbotron" align="center">
        <h1>{{$kelas->nama}}</h1>
        <p>...</p>
        <p>{{$kelas->deskripsi}}</p>
        @if(!Auth::user()->id == @$users->teacher)
            <h4>student</h4>
        @else
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Buat Tugas
          </button>
          
          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Tugas</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    {!!Form::open(['url'=>'task','files'=>'true'])!!}
                    <div class="form-group">
                        {{Form::label('judul','Judul',['class'=>'controll-label'])}}
                        {{Form::text('judul','',['class'=>'form-control','required'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('deskripsi','Deskripsi',['class'=>'controll-label'])}}
                        {{Form::text('deskripsi','',['class'=>'form-control','required'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('topik','Topik',['class'=>'controll-label'])}}
                        @if(count($topic_list)>0)
                            {{Form::select('topic',$topic_list,null,['class'=>'form-control','id'=>'Topik','placeholder'=>'Pilih Topic'])}}
                        @else
                            <p>Tidak ada Topic</p>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::label('tenggat_waktu','Tenggat Waktu (optional)',['class'=>'control-label'])}}
                        {{Form::date('tenggat_waktu',null,['class'=>'form-control','id'=>'tenggat_waktu'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('file','Lampirkan File (optional)',['class'=>'control-label'])}}
                        {!!Form::file('file',['class'=>'form-control','id'=>'file','placeholder'=>'Optional'])!!}
                    </div>
                    {{Form::hidden('kelas_id', $kelas->id )}}
                    {{Form::hidden('user_id', Auth::user()->id )}}
                </div>
                <div class="modal-footer ">
                  <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                  {{Form::submit('Simpan',['class'=>'btn btn-primary '])}}
                {!!Form::close()!!}
                </div>
              </div>
            </div>
          </div>
        @endif
    </div>
    <div class="container">
        <div class="row">
                <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">Topik</div>
                            <div class="card-body">
                                @if (count($topics)>0)
                                    @foreach($topics as $topic)
                                        <ul>
                                            <li>{{$topic->topic}}</li>
                                        </ul>
                                    @endforeach
                                @else
                                    Tidak ada topic
                                @endif
                            </div>
                            <div class="card-footer">
                            @if(Auth::user()->id == @$users->teacher)
                                <button type="button" class="btn btn-primary right" data-toggle="modal" data-target="#exampleModal2">
                                    Buat Topik
                                </button>
                            @endif
                                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Topik</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                {!!Form::open(['url'=>'topic'])!!}
                                                    <div class="form-group">
                                                        {{Form::label('topik','Nama Topik')}}
                                                        {{Form::text('topic','',['class'=>'form-control','required'])}}
                                                    </div>
                                                    {{Form::hidden('kelas_id', $kelas->id )}}
                                            </div>
                                            <div class="modal-footer right">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    {{Form::submit('Simpan',['class'=>'btn btn-primary'])}}
                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-9">
                        @if(count($tasks_list)>0)
                            @foreach($tasks_list as $task)
                                @if(Auth::user()->id == @$users->teacher)
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-11">
                                                    <a href="/task_detail/{{$task->id}}">{{$task->judul}}</a><br>
                                                    <h6>{{$task->created_at->format('d-m-Y')}}</h6>
                                                </div>
                                                <div class="col-1">
                                                  <a href="/destroy/{{$task->id}}" class="btn btn-danger"><i class="fa fa-trash"> </i></a>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-6">
                                                    @if(count($task->deadline)>0)
                                                    Batas Waktu {{$task->deadline->format('d-m-Y')}}<br>
                                                    @endif
                                                    <h3>{{$task->deskripsi}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-9">
                                                    <a href="/task_detail/{{$task->id}}">{{$task->judul}}</a><br>
                                                    <h6>{{$task->created_at->format('d-m-Y')}}</h6>
                                                </div>
                                                <div class="col-3">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            {{$task->deskripsi}}
                                        </div>
                                    </div>
                                @endif
                            <br>
                            @endforeach
                        @else
                            <p>Tidak Ada tugas</p>
                        @endif
                    </div>
        </div>
    </div>
@endsection
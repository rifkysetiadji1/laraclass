@extends('../layouts/app')
@section('content')
<div class="container">
    @if (!empty($kelas_list))
        <?php foreach ($kelas_list as $kelas): ?>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">{{$kelas->nama}}</div>
                </div>
                
            </div>
            
        <?php endforeach ?>
    @else
        <p>Tidak Ada Data kelas.</p>
    @endif
</div>
@endsection
@extends('../layouts/app')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Kelas Baru</div>
        
                        <div class="card-body">
                            {!!Form::open(['url'=>'kelas'])!!}
                                <div class="form-group">
                                    {{Form::label('nama','Nama Kelas')}}
                                    {{Form::text('nama','',['class'=>'form-control','required'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('deskripsi','Deskripsi Kelas')}}
                                    {{Form::text('deskripsi','',['class'=>'form-control','required'])}}
                                </div>
                               
                                    {{Form::hidden('id_user', Auth::user()->id )}}
                                
                                {{Form::submit('Simpan',['class'=>'btn btn-primary'])}}
                            {!!Form::close()!!}
                        </div>
                    </div>
            </div>
        </div>
</div>
@endsection
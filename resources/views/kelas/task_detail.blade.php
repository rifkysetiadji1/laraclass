@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <div class="container">
            <h3>{{$tasks->judul}}</h3>
            <h6>{{$tasks->created_at->format('d-m-Y')}}</h6>
        </div>
    </div>
    @if(Auth::user()->id == @$users->teacher)
        <div class="container" align="center">
            @if(count(@$jumlah_user)>0)
                <h3>jumlah yang selesai {{@$jumlah_user}}</h3>
                <div class="row justify-content-center">
                        <div class="col-md-6">
                            <ul class="list-group">
                                @foreach($workes as $work)
                                    <li class="list-group-item">{{$work->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                </div>
            @else
                <h3>Belum ada yang selesai</h3>
            @endif
        </div>
    @else
        <div class="container" align="center">
            <p>{{$tasks->deskripsi}}</p>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card card-succes">
                        <div class="card-body">
                            {{$tasks->file}} | <a href="/file_download/{{$tasks->file}}" class="btn btn-danger "><i class="fa fa-download"> </i></a>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8 pull-left">
                                    
                                </div>
                                <div class="col-4">
                                @if(count($work)>0)
                                    <h3>Done</h3>
                                @else
                                    <h3>Not Done</h3>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            Clik selesai atau lampirkan File
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-8 pull-left">
                                    {!!Form::open(['url'=>'/work','files'=>'true'])!!}
                                    {!!Form::file('file',['class'=>'form-control','id'=>'file'])!!}
                                    {{Form::hidden('task_id', $tasks->id )}}
                                    {{Form::hidden('kelas_id', $tasks->kelas_id )}}
                                    {{Form::hidden('user_id', Auth::user()->id )}}
                                </div>
                                <div class="col-4">
                                    @if(!count($work)>0)
                                        {{Form::submit('Selesai',['class'=>'btn btn-primary '])}}
                                    @endif
                                    {!!Form::close()!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
@endsection
@extends('../layouts/app')
@section('content')
<div class="container">
   
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Join Kelas</div>

                <div class="card-body">
                    {!!Form::open(['url'=>'join_kode'])!!}
                        <div class="form-group">
                            {{Form::label('nama','Masukan Kode Kelas')}}
                            {{Form::text('kode_kelas','',['class'=>'form-control','required'])}}
                        </div>
                        
                        {{Form::submit('OK',['class'=>'btn btn-primary'])}}
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
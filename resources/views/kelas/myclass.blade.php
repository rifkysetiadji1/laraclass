@extends('layouts.app')

@section('content')
<div class="container">
    @if (count($class)>0)
      <div class="row">
        <?php foreach ($class as $clas): ?>
            <div class="col-sm-3">
                <div class="card">
                
                <div class="card-header"><a href="/show/{{$clas->id}}">{{$clas->nama}}</a></div>
                    <div class="card-body">
                        {{$clas->deskripsi}}
                        
                        
                    </div>
                </div>
            </div>
            <br>
        <?php endforeach ?>
     </div>
    @else
    <h2>Tidak ada Kelas</h2>
    @endif
    
</div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', 'KelasController@index');
Route::get('/myclass', 'KelasController@myclass');
Route::get('/classroom', 'KelasController@classroom');
Route::get('/create','KelasController@create');
Route::get('/join','KelasController@join');
Route::post('/kelas','KelasController@store');
Route::get('/show/{kelas}','KelasController@show');
Route::post('/join_kode','KelasController@join_kode');

Route::post('/topic','TopicController@store');

Route::post('/task','TaskController@store');
Route::get('/task_detail/{task}','TaskController@show');
Route::get('/file_download/{file}','TaskController@download');
Route::get('/destroy/{task}','TaskController@destroy');
Route::post('/work','TaskController@work');

